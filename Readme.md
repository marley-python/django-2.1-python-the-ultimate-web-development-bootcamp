# Django 2.1 & Python | The Ultimate Web Development Bootcamp

    $ django-admin startproject wordcount
    $ cd wordcount
    $ python3 manage.py runserver

<br/>

## Site #1 - Word Counter

<br/>

## Site #2 - Your Personal Portfolio

    $ sudo apt install python3-pip
    $ pip3 install virtualenv
    $ virtualenv virtenv
    $ source venv/bin/activate

    // if needed
    $ deactivate

    $ django-admin startproject portfolio

    $ pip install django==2.1.7

    $ mv portfolio portfolio-project
    $ cd portfolio-project
    $ python3 manage.py runserver

    $ python3 manage.py startapp blog
    $ python3 manage.py startapp jobs

<br/>

### 6. Models

    $ pip3 install Pillow

    $ python3 manage.py makemigrations
    $ python3 manage.py migrate

<br/>

### 7. Admin

    $ python3 manage.py createsuperuser
    $ python3 manage.py runserver

http://localhost:8000/admin/

<br/>

### 9. Postgres

    psql
    \du

    \password postgres
    CREATE DATABASE portfoliodb;

    $ pip3 install psycopg2
    $ pip3 install psycopg2-binary
    $ python3 manage.py migrate

    $ python3 manage.py createsuperuser
    $ python3 manage.py runserver

http://localhost:8000/admin/

<br/>

### 10. Test Your Skills - Blog Model

    $ python3 manage.py makemigrations
    $ python3 manage.py migrate

    $ python3 manage.py runserver

http://localhost:8000/admin/

<br/>

### 11. Home Page

<br/>

### 12. Bootstrap

https://getbootstrap.com/docs/4.3/examples/album/

<br/>

### 13. Show Jobs

<br/>

### 14. All Blogs

<br/>

### 15. Blog Detail

http://localhost:8000/blog/1/

<br/>

### 16. Static Files

    $ python3 manage.py collectstatic

<br/>

### 17. Polish

<br/>

## Site #3 - Product Hunt Clone

original src:  
https://github.com/zappycode/producthunt-project

<br/>

### 2. Sketch

    $ django-admin startproject producthunt
    $ mv producthunt producthunt-project

set postgresdb connection string

    $ python3 manage.py migrate
    $ python3 manage.py runserver 0.0.0.0:8080

<br/>

### 3. Extending Templates

    $ python3 manage.py startapp products
    $ python3 manage.py startapp accounts

    $ python3 manage.py runserver 0.0.0.0:8080

<br/>

### 4. Base Styling

    $ python3 manage.py collectstatic

<br/>

### 5. Sign Up

    $ python3 manage.py createsuperuser

<br/>

### 6. Login and Logout

<br/>

### 7. Products Model

    $ python3 manage.py makemigrations
    $ python3 manage.py migrate
    $ python3 manage.py runserver 0.0.0.0:8080

<br/>

### 8. Creating Products

<br/>

### 9. Iconic

    $ python3 manage.py collectstatic

<br/>

### 10. Product Details

http://localhost:8080/products/1

<br/>

### 11. Home Page

<br/>

### 12. Polish
